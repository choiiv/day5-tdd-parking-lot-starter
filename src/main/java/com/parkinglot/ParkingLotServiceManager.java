package com.parkinglot;

import java.util.List;
import java.util.Optional;

public class ParkingLotServiceManager extends ParkingBoy {
    private List<ParkingBoy> parkingBoys;

    public ParkingLotServiceManager(List<ParkingBoy> parkingBoys) {
        super(null, null);
        this.parkingBoys = parkingBoys;
    }

    public ParkingLotServiceManager(List<ParkingBoy> parkingBoys, List<ParkingLot> parkingLots, IParkingLotSearchStrategy strategy) {
        super(parkingLots, strategy);
        this.parkingBoys = parkingBoys;
    }

    public ParkingTicket parkByParkingBoy(Car car) {
        try {
            Optional<ParkingBoy> availableParkingBoy = parkingBoys.stream()
                    .filter(ParkingBoy::haveAvailableParkingLot)
                    .findFirst();
            if (availableParkingBoy.isEmpty()) {
                throw new NoAvailablePositionException("No available position.");
            }
            return availableParkingBoy.get().park(car);
        } catch (RuntimeException exception) {
            throw exception;
        }
    }

    public Car fetchByParkingBoy(ParkingTicket ticket) {
        try {
            Optional<ParkingBoy> availableParkingBoy = parkingBoys.stream()
                    .filter(parkingBoy -> parkingBoy.knowsTicket(ticket))
                    .findFirst();
            if (availableParkingBoy.isEmpty()) {
                throw new NoAvailablePositionException("Unrecognized parking ticket.");
            }
            return availableParkingBoy.get().fetch(ticket);
        } catch (RuntimeException exception) {
            throw exception;
        }
    }
}
