package com.parkinglot;

import java.util.HashMap;
import java.util.List;

public class ParkingBoy {
    private List<ParkingLot> parkingLots;
    private IParkingLotSearchStrategy parkingLotSearchStrategy;
    private HashMap<ParkingTicket, ParkingLot> parkingLotOfTicket = new HashMap<>();

    public ParkingBoy(List<ParkingLot> parkingLots, IParkingLotSearchStrategy parkingLotSearchStrategy) {
        this.parkingLots = parkingLots;
        this.parkingLotSearchStrategy = parkingLotSearchStrategy;
    }

    public ParkingTicket park(Car car) {
        ParkingLot availableParkingLot = parkingLotSearchStrategy.findParkingLot(parkingLots);
        if (availableParkingLot == null) {
            throw new NoAvailablePositionException("No available position.");
        }
        ParkingTicket ticket = availableParkingLot.park(car);
        if (ticket != null) {
            parkingLotOfTicket.put(ticket, availableParkingLot);
        }
        return ticket;
    }

    public Car fetch(ParkingTicket ticket) {
        ParkingLot targetParkingLot = parkingLotOfTicket.remove(ticket);
        if (targetParkingLot == null) {
            throw new UnrecognizedTicketException("Unrecognized parking ticket.");
        }
        return targetParkingLot.fetch(ticket);
    }

    public boolean haveAvailableParkingLot() {
        return parkingLotSearchStrategy.findParkingLot(parkingLots) != null;
    }

    public boolean knowsTicket(ParkingTicket ticket) {
        return parkingLotOfTicket.containsKey(ticket);
    }
}
