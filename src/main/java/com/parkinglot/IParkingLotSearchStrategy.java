package com.parkinglot;

import java.util.List;

public interface IParkingLotSearchStrategy {
    ParkingLot findParkingLot(List<ParkingLot> parkingLots);
}
