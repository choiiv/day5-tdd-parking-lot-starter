package com.parkinglot;

import java.util.List;

public class SuperParkingLotSearchStrategy implements IParkingLotSearchStrategy {
    @Override
    public ParkingLot findParkingLot(List<ParkingLot> parkingLots) {
        return parkingLots.stream()
                .reduce(null, (foundTargetParkingLot, currentParkingLot) -> {
                    if (foundTargetParkingLot != null) {
                        double currentParkingLotAvailablePositionRate = currentParkingLot.getRemainingSize() / (double) currentParkingLot.getCapacity();
                        double foundTargetParkingLotAvailablePositionRate = foundTargetParkingLot.getRemainingSize() / (double) foundTargetParkingLot.getCapacity();
                        if (foundTargetParkingLotAvailablePositionRate >= currentParkingLotAvailablePositionRate) {
                            return foundTargetParkingLot;
                        }
                    }
                    return currentParkingLot;
                });
    }
}
