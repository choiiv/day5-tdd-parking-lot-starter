package com.parkinglot;

import java.util.HashMap;

public class ParkingLot {
    private HashMap<ParkingTicket, Car> tickets = new HashMap<>();
    int capacity = 10;

    public ParkingLot() {

    }

    public ParkingLot(int capacity) {
        this.capacity = capacity;
    }

    public ParkingTicket park(Car car) {
        if (tickets.size() == capacity) {
            throw new NoAvailablePositionException("No available position.");
        }
        ParkingTicket ticket = new ParkingTicket();
        tickets.put(ticket, car);
        return ticket;
    }

    public Car fetch(ParkingTicket ticket) {
        if (!tickets.containsKey(ticket)) {
            throw new UnrecognizedTicketException("Unrecognized parking ticket.");
        }
        return tickets.remove(ticket);
    }

    public int getRemainingSize() {
        return capacity - tickets.size();
    }

    public int getCapacity() {
        return capacity;
    }
}
