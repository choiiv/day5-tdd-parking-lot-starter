package com.parkinglot;

import java.util.List;

public class SmartParkingLotSearchStrategy implements IParkingLotSearchStrategy {
    @Override
    public ParkingLot findParkingLot(List<ParkingLot> parkingLots) {
        return parkingLots.stream()
                .reduce(null, (foundTargetParkingLot, currentParkingLot) -> {
                    if (foundTargetParkingLot != null && foundTargetParkingLot.getRemainingSize() >= currentParkingLot.getRemainingSize()) {
                        return foundTargetParkingLot;
                    }
                    return currentParkingLot;
                });
    }
}
