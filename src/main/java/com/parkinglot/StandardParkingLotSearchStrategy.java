package com.parkinglot;

import java.util.List;
import java.util.Optional;

public class StandardParkingLotSearchStrategy implements IParkingLotSearchStrategy {

    @Override
    public ParkingLot findParkingLot(List<ParkingLot> parkingLots) {
        Optional<ParkingLot> availableParkingLot = parkingLots.stream()
                .filter(parkingLot -> parkingLot.getRemainingSize() > 0)
                .findFirst();
        return availableParkingLot.orElse(null);
    }
}
