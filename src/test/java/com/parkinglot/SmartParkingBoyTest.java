package com.parkinglot;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SmartParkingBoyTest {
    @Test
    void should_park_in_second_parking_lot_when_park_given_smart_parking_boy_and_two_parking_lots_with_latter_larger_empty_space_and_a_car() {
        //given
        ParkingLot firstParkingLot = new ParkingLot(10);
        ParkingLot secondParkingLot = new ParkingLot(20);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(firstParkingLot);
        parkingLots.add(secondParkingLot);
        IParkingLotSearchStrategy smartParkingLotSearchStrategy = new SmartParkingLotSearchStrategy();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLots, smartParkingLotSearchStrategy);
        Car car = new Car();

        //when
        ParkingTicket ticket = parkingBoy.park(car);

        //then
        assertEquals(19, secondParkingLot.getRemainingSize());
    }

    @Test
    void should_park_in_second_parking_lot_when_park_given_smart_parking_boy_and_first_full_and_second_available_parking_lots_and_a_car() {
        //given
        ParkingLot firstParkingLot = new ParkingLot(1);
        ParkingLot secondParkingLot = new ParkingLot(1);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(firstParkingLot);
        parkingLots.add(secondParkingLot);
        IParkingLotSearchStrategy smartParkingLotSearchStrategy = new SmartParkingLotSearchStrategy();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLots, smartParkingLotSearchStrategy);
        Car firstCar = new Car();
        Car secondCar = new Car();
        parkingBoy.park(firstCar);
        // is this ok?
        assertEquals(1, secondParkingLot.getRemainingSize());
        //when
        parkingBoy.park(secondCar);
        //then
        assertEquals(0, secondParkingLot.getRemainingSize());
    }

    @Test
    void should_return_no_available_position_exception_when_park_given_smart_parking_boy_and_two_full_parking_lots() {
        //given
        ParkingLot firstParkingLot = new ParkingLot(1);
        ParkingLot secondParkingLot = new ParkingLot(1);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(firstParkingLot);
        parkingLots.add(secondParkingLot);
        IParkingLotSearchStrategy smartParkingLotSearchStrategy = new SmartParkingLotSearchStrategy();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLots, smartParkingLotSearchStrategy);
        Car car1 = new Car();
        Car car2 = new Car();
        Car car3 = new Car();
        parkingBoy.park(car1);
        parkingBoy.park(car2);
        //then
        Throwable exception = assertThrows(NoAvailablePositionException.class, () -> {
            parkingBoy.park(car3);
        });
        assertEquals("No available position.", exception.getMessage());
    }
}
