package com.parkinglot;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.*;

public class StandardParkingBoyTest {
    @Test
    void should_park_in_first_parking_lot_when_park_given_standard_parking_boy_and_two_parking_lots_and_a_car() {
        //given
        ParkingLot firstParkingLot = new ParkingLot(10);
        ParkingLot secondParkingLot = new ParkingLot(10);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(firstParkingLot);
        parkingLots.add(secondParkingLot);
        IParkingLotSearchStrategy standardParkingLotSearchStrategy = new StandardParkingLotSearchStrategy();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLots, standardParkingLotSearchStrategy);
        Car car = new Car();

        //when
        ParkingTicket ticket = parkingBoy.park(car);

        //then
        assertEquals(9, firstParkingLot.getRemainingSize());
    }

    @Test
    void should_park_in_second_parking_lot_when_park_given_standard_parking_boy_and_first_full_and_second_available_parking_lots_and_a_car() {
        //given
        ParkingLot firstParkingLot = new ParkingLot(1);
        ParkingLot secondParkingLot = new ParkingLot(1);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(firstParkingLot);
        parkingLots.add(secondParkingLot);
        IParkingLotSearchStrategy standardParkingLotSearchStrategy = new StandardParkingLotSearchStrategy();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLots, standardParkingLotSearchStrategy);
        Car firstCar = new Car();
        Car secondCar = new Car();
        parkingBoy.park(firstCar);
        //when
        parkingBoy.park(secondCar);
        //then
        assertEquals(0, secondParkingLot.getRemainingSize());
    }


    @Test
    void should_return_no_available_position_exception_when_park_given_standard_parking_boy_and_two_full_parking_lots() {
        //given
        ParkingLot firstParkingLot = new ParkingLot(1);
        ParkingLot secondParkingLot = new ParkingLot(1);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(firstParkingLot);
        parkingLots.add(secondParkingLot);
        IParkingLotSearchStrategy standardParkingLotSearchStrategy = new StandardParkingLotSearchStrategy();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLots, standardParkingLotSearchStrategy);
        Car car1 = new Car();
        Car car2 = new Car();
        Car car3 = new Car();
        parkingBoy.park(car1);
        parkingBoy.park(car2);
        //then
        Throwable exception = assertThrows(NoAvailablePositionException.class, () -> {
            parkingBoy.park(car3);
        });
        assertEquals("No available position.", exception.getMessage());
    }
}
