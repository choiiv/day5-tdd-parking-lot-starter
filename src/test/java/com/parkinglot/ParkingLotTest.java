package com.parkinglot;

import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

public class ParkingLotTest {
    @Test
    void should_return_ticket_when_park_given_parking_lot_and_a_car() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();

        //when
        ParkingTicket ticket = parkingLot.park(car);

        //then
        assertNotNull(ticket);
    }

    @Test
    void should_return_car_when_fetch_given_parking_lot_with_a_parked_car_by_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
        ParkingTicket ticket = parkingLot.park(car);
        //when
        Car actual = parkingLot.fetch(ticket);
        //then
        assertEquals(car, actual);
    }

    @Test
    void should_return_right_cars_when_fetch_twice_given_parking_lot_with_two_parked_cars_by_two_tickets() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car firstCar = new Car();
        Car secondCar = new Car();
        ParkingTicket firstTicket = parkingLot.park(firstCar);
        ParkingTicket secondTicket = parkingLot.park(secondCar);
        //when
        Car actualFirst = parkingLot.fetch(firstTicket);
        Car actualSecond = parkingLot.fetch(secondTicket);
        //then
        assertEquals(firstCar, actualFirst);
        assertEquals(secondCar, actualSecond);
    }

    @Test
    void should_return_unrecognized_ticket_exception_when_fetch_given_parking_lot_with_a_parked_car_by_wrong_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
        parkingLot.park(car);
        ParkingTicket ticket = new ParkingTicket();
        //then
        Throwable exception = assertThrows(UnrecognizedTicketException.class, () -> {
            parkingLot.fetch((ticket));
        });
        assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }
    
    @Test
    void should_return_unrecognized_ticket_exception_when_fetch_given_parking_lot_with_a_parked_car_by_used_ticket() {
        //given
        ParkingLot parkingLot = new ParkingLot();
        Car car = new Car();
        ParkingTicket ticket = parkingLot.park(car);

        //then
        Throwable exception = assertThrows(UnrecognizedTicketException.class, () -> {
            parkingLot.fetch(ticket);
            parkingLot.fetch(ticket);
        });
        assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }

    @Test
    void should_return_no_available_position_exception_when_park_given_full_parking_lot() {
        //given
        ParkingLot parkingLot = new ParkingLot(2);
        Car car1 = new Car();
        Car car2 = new Car();
        Car car3 = new Car();
        parkingLot.park(car1);
        parkingLot.park(car2);
        //then
        Throwable exception = assertThrows(NoAvailablePositionException.class, () -> {
            parkingLot.park(car3);
        });
        assertEquals("No available position.", exception.getMessage());
    }
}
