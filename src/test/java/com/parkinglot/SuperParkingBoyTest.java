package com.parkinglot;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class SuperParkingBoyTest {
    @Test
    void should_park_in_second_parking_lot_when_park_given_super_parking_boy_and_second_parking_lot_has_higher_available_position_rate_and_a_car() {
        //given
        ParkingLot firstParkingLot = new ParkingLot(2);
        ParkingLot secondParkingLot = new ParkingLot(3);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(firstParkingLot);
        parkingLots.add(secondParkingLot);
        IParkingLotSearchStrategy superParkingLotSearchStrategy = new SuperParkingLotSearchStrategy();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLots, superParkingLotSearchStrategy);
        Car firstCar = new Car();
        Car secondCar = new Car();
        parkingBoy.park(firstCar);
        //when
        ParkingTicket ticket = parkingBoy.park(secondCar);

        //then
        assertEquals(2, secondParkingLot.getRemainingSize());
    }

    @Test
    void should_park_in_second_parking_lot_when_park_given_super_parking_boy_and_first_full_and_second_available_parking_lots_and_a_car() {
        //given
        ParkingLot firstParkingLot = new ParkingLot(1);
        ParkingLot secondParkingLot = new ParkingLot(1);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(firstParkingLot);
        parkingLots.add(secondParkingLot);
        IParkingLotSearchStrategy superParkingLotSearchStrategy = new SuperParkingLotSearchStrategy();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLots, superParkingLotSearchStrategy);
        Car firstCar = new Car();
        Car secondCar = new Car();
        parkingBoy.park(firstCar);
        assertEquals(1, secondParkingLot.getRemainingSize());
        //when
        parkingBoy.park(secondCar);
        //then
        assertEquals(0, secondParkingLot.getRemainingSize());
    }

    @Test
    void should_return_no_available_position_exception_when_park_given_super_parking_boy_and_two_full_parking_lots() {
        //given
        ParkingLot firstParkingLot = new ParkingLot(1);
        ParkingLot secondParkingLot = new ParkingLot(1);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(firstParkingLot);
        parkingLots.add(secondParkingLot);
        IParkingLotSearchStrategy superParkingLotSearchStrategy = new SuperParkingLotSearchStrategy();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLots, superParkingLotSearchStrategy);
        Car car1 = new Car();
        Car car2 = new Car();
        Car car3 = new Car();
        parkingBoy.park(car1);
        parkingBoy.park(car2);
        //then
        Throwable exception = assertThrows(NoAvailablePositionException.class, () -> {
            parkingBoy.park(car3);
        });
        assertEquals("No available position.", exception.getMessage());
    }
}
