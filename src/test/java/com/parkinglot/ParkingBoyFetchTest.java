package com.parkinglot;

import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ParkingBoyFetchTest {
    @Test
    void should_fetch_two_correct_cars_when_fetch_given_parking_boy_and_two_parking_lots_and_a_car() {
        //given
        ParkingLot firstParkingLot = new ParkingLot(1);
        ParkingLot secondParkingLot = new ParkingLot(1);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(firstParkingLot);
        parkingLots.add(secondParkingLot);
        IParkingLotSearchStrategy standardParkingLotSearchStrategy = new StandardParkingLotSearchStrategy();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLots, standardParkingLotSearchStrategy);
        Car firstCar = new Car();
        Car secondCar = new Car();
        ParkingTicket firstTicket = parkingBoy.park(firstCar);
        ParkingTicket secondTicket = parkingBoy.park(secondCar);
        //when
        Car actualFirst = parkingBoy.fetch(firstTicket);
        Car actualSecond = parkingBoy.fetch(secondTicket);
        //then
        assertEquals(firstCar, actualFirst);
        assertEquals(secondCar, actualSecond);
    }

    @Test
    void should_return_unrecognized_ticket_exception_when_fetch_given_parking_boy_and_two_parking_lots_with_a_parked_car_by_wrong_ticket() {
        //given
        ParkingLot firstParkingLot = new ParkingLot(1);
        ParkingLot secondParkingLot = new ParkingLot(1);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(firstParkingLot);
        parkingLots.add(secondParkingLot);
        IParkingLotSearchStrategy standardParkingLotSearchStrategy = new StandardParkingLotSearchStrategy();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLots, standardParkingLotSearchStrategy);
        Car car = new Car();
        parkingBoy.park(car);
        ParkingTicket wrongTicket = new ParkingTicket();

        //then
        Throwable exception = assertThrows(UnrecognizedTicketException.class, () -> {
            parkingBoy.fetch(wrongTicket);
        });
        assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }

    @Test
    void should_return_unrecognized_ticket_exception_when_fetch_given_parking_boy_and_two_parking_lots_with_a_parked_car_by_used_ticket() {
        //given
        ParkingLot firstParkingLot = new ParkingLot(1);
        ParkingLot secondParkingLot = new ParkingLot(1);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(firstParkingLot);
        parkingLots.add(secondParkingLot);
        IParkingLotSearchStrategy standardParkingLotSearchStrategy = new StandardParkingLotSearchStrategy();
        ParkingBoy parkingBoy = new ParkingBoy(parkingLots, standardParkingLotSearchStrategy);
        Car car = new Car();
        ParkingTicket ticket = parkingBoy.park(car);

        //then
        Throwable exception = assertThrows(UnrecognizedTicketException.class, () -> {
            parkingBoy.fetch(ticket);
            parkingBoy.fetch(ticket);
        });
        assertEquals("Unrecognized parking ticket.", exception.getMessage());
    }
}
