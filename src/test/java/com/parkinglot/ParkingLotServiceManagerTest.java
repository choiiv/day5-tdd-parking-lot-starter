package com.parkinglot;

import org.junit.jupiter.api.Test;
import org.mockito.Mockito;

import java.util.ArrayList;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;

public class ParkingLotServiceManagerTest {
    @Test
    void should_park_in_first_parking_lot_of_first_parking_boy_when_park_by_parking_boy_given_manager_and_two_parking_boy_each_having_two_empty_parking_lots_and_a_car() {
        //given
        List<ParkingLot> firstParkingLots = List.of(new ParkingLot(10), new ParkingLot(10));
        List<ParkingLot> secondParkingLots = List.of(new ParkingLot(10), new ParkingLot(10));
        ParkingBoy firstParkingBoy = new ParkingBoy(firstParkingLots, new StandardParkingLotSearchStrategy());
        ParkingBoy secondParkingBoy = new ParkingBoy(secondParkingLots, new StandardParkingLotSearchStrategy());
        ParkingLotServiceManager manager = new ParkingLotServiceManager(List.of(firstParkingBoy, secondParkingBoy));
        Car car = new Car();

        //when
        manager.parkByParkingBoy(car);

        //then
        assertEquals(9, firstParkingLots.get(0).getRemainingSize());
    }

    @Test
    void should_park_in_first_parking_lot_of_second_parking_boy_when_park_by_parking_boy_given_manager_and_first_parking_boy_full_and_second_parking_boy_empty_and_a_car() {
        //given
        List<ParkingLot> firstParkingLots = List.of(new ParkingLot(1), new ParkingLot(1));
        List<ParkingLot> secondParkingLots = List.of(new ParkingLot(1), new ParkingLot(1));
        ParkingBoy firstParkingBoy = new ParkingBoy(firstParkingLots, new StandardParkingLotSearchStrategy());
        ParkingBoy secondParkingBoy = new ParkingBoy(secondParkingLots, new StandardParkingLotSearchStrategy());
        ParkingLotServiceManager manager = new ParkingLotServiceManager(List.of(firstParkingBoy, secondParkingBoy));
        Car firstCar = new Car();
        Car secondCar = new Car();
        Car thirdCar = new Car();
        manager.parkByParkingBoy(firstCar);
        manager.parkByParkingBoy(secondCar);
        assertEquals(1, secondParkingLots.get(0).getRemainingSize());
        //when
        manager.parkByParkingBoy(thirdCar);

        //then
        assertEquals(0, secondParkingLots.get(0).getRemainingSize());
    }

    @Test
    void should_return_no_available_position_exception_when_park_by_parking_boy_given_manager_and_two_full_parking_boys() {
        //given
        List<ParkingLot> firstParkingLots = List.of(new ParkingLot(1), new ParkingLot(1));
        List<ParkingLot> secondParkingLots = List.of(new ParkingLot(1), new ParkingLot(1));
        ParkingBoy firstParkingBoy = new ParkingBoy(firstParkingLots, new StandardParkingLotSearchStrategy());
        ParkingBoy secondParkingBoy = new ParkingBoy(secondParkingLots, new StandardParkingLotSearchStrategy());
        ParkingLotServiceManager manager = new ParkingLotServiceManager(List.of(firstParkingBoy, secondParkingBoy));
        for (int i = 0; i < 4; i++) {
            manager.parkByParkingBoy(new Car());
        }

        //then
        Throwable exception = assertThrows(NoAvailablePositionException.class, () -> {
            manager.parkByParkingBoy(new Car());
        });
        assertEquals("No available position.", exception.getMessage());
    }

    @Test
    void should_park_in_first_parking_lot_when_park_by_manager_given_manager_and_two_parking_lots_and_a_car() {
        //given
        ParkingLot firstParkingLot = new ParkingLot(10);
        ParkingLot secondParkingLot = new ParkingLot(10);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(firstParkingLot);
        parkingLots.add(secondParkingLot);
        IParkingLotSearchStrategy standardParkingLotSearchStrategy = new StandardParkingLotSearchStrategy();
        ParkingLotServiceManager manager = new ParkingLotServiceManager(null, parkingLots, standardParkingLotSearchStrategy);
        Car car = new Car();

        //when
        ParkingTicket ticket = manager.park(car);

        //then
        assertEquals(9, firstParkingLot.getRemainingSize());
    }

    @Test
    void should_park_in_second_parking_lot_when_park_by_manager_given_manager_and_first_full_and_second_available_parking_lots_and_a_car() {
        //given
        ParkingLot firstParkingLot = new ParkingLot(1);
        ParkingLot secondParkingLot = new ParkingLot(1);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(firstParkingLot);
        parkingLots.add(secondParkingLot);
        IParkingLotSearchStrategy standardParkingLotSearchStrategy = new StandardParkingLotSearchStrategy();
        ParkingLotServiceManager manager = new ParkingLotServiceManager(null, parkingLots, standardParkingLotSearchStrategy);
        Car firstCar = new Car();
        Car secondCar = new Car();
        manager.park(firstCar);
        //when
        manager.park(secondCar);
        //then
        assertEquals(0, secondParkingLot.getRemainingSize());
    }

    @Test
    void should_return_no_available_position_exception_when_park_by_manager_given_manager_and_two_full_parking_lots() {
        //given
        ParkingLot firstParkingLot = new ParkingLot(1);
        ParkingLot secondParkingLot = new ParkingLot(1);
        List<ParkingLot> parkingLots = new ArrayList<>();
        parkingLots.add(firstParkingLot);
        parkingLots.add(secondParkingLot);
        IParkingLotSearchStrategy standardParkingLotSearchStrategy = new StandardParkingLotSearchStrategy();
        ParkingLotServiceManager manager = new ParkingLotServiceManager(null, parkingLots, standardParkingLotSearchStrategy);
        Car car1 = new Car();
        Car car2 = new Car();
        Car car3 = new Car();
        manager.park(car1);
        manager.park(car2);
        //then
        Throwable exception = assertThrows(NoAvailablePositionException.class, () -> {
            manager.park(car3);
        });
        assertEquals("No available position.", exception.getMessage());
    }

    @Test
    void should_fetch_correct_car_when_fetch_by_parking_boy_given_two_parking_boy_and_correct_ticket() {
        //given
        ParkingLot firstParkingLot = new ParkingLot(1);
        ParkingLot secondParkingLot = new ParkingLot(1);
        IParkingLotSearchStrategy standardParkingLotSearchStrategy = new StandardParkingLotSearchStrategy();
        ParkingBoy firstParkingBoy = new ParkingBoy(List.of(firstParkingLot), standardParkingLotSearchStrategy);
        ParkingBoy secondParkingBoy = new ParkingBoy(List.of(secondParkingLot), standardParkingLotSearchStrategy);
        Car car = new Car();
        ParkingTicket ticket =  firstParkingBoy.park(car);
        ParkingLotServiceManager manager = new ParkingLotServiceManager(List.of(firstParkingBoy, secondParkingBoy));
        //when
        Car actual = manager.fetchByParkingBoy(ticket);
        //then
        assertEquals(car, actual);
    }

    @Test
    void should_fetch_two_correct_cars_when_fetch_by_parking_boy_given_two_parking_boy_and_correct_tickets() {
        //given
        ParkingLot firstParkingLot = new ParkingLot(1);
        ParkingLot secondParkingLot = new ParkingLot(1);
        IParkingLotSearchStrategy standardParkingLotSearchStrategy = new StandardParkingLotSearchStrategy();
        ParkingBoy firstParkingBoy = new ParkingBoy(List.of(firstParkingLot), standardParkingLotSearchStrategy);
        ParkingBoy secondParkingBoy = new ParkingBoy(List.of(secondParkingLot), standardParkingLotSearchStrategy);
        Car firstCar = new Car();
        Car secondCar = new Car();
        ParkingTicket firstTicket =  firstParkingBoy.park(firstCar);
        ParkingTicket secondTicket =  secondParkingBoy.park(secondCar);
        ParkingLotServiceManager manager = new ParkingLotServiceManager(List.of(firstParkingBoy, secondParkingBoy));
        //when
        Car actualFirst = manager.fetchByParkingBoy(firstTicket);
        Car actualSecond = manager.fetchByParkingBoy(secondTicket);
        //then
        assertEquals(firstCar, actualFirst);
        assertEquals(secondCar, actualSecond);
    }
}
